# Physical model of thermal expansion of selected materials
> The main goal of one of firsts project is to calculate extension of some provided materials (solid or liquid)

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Screenshot](#screenshot)
* [Project Status](#project-status)
* [Room for Improvement](#room-for-improvement)
* [Contact](#contact)
<!-- * [License](#license) -->


## General Information
- This project was realized with colleague on III semester of studies.
- Program calculate extension of some popular materials (solid or liquid).
- The purpose of project was to learn Python in use.


## Technologies Used
- Python - version 3.9
- PyCharm Community Edition


## Features
Provided features:
- Calculate extension depend of begin and end temperature
- Draw a linear plot with coefficient of expansion
- Option to save calculated values or a plot to a file
- Easy way to change material or one of variables (lenght, temperature)


## Screenshot
![Example screenshot](./ScreenshotOfProgram.jpg)
<!-- If you have screenshots you'd like to share, include them here. -->


## Project Status
Project is: _no longer being worked on_. 


## Room for Improvement
To do:
- Update code to new Python version.
- Make code clear.
- Write better calsses.



## Contact
Created by Piotr Hajduk and Anna Kujawa


<!-- Optional -->
<!-- ## License -->
<!-- This project is open source and available under the [... License](). -->

<!-- You don't have to include all sections - just the one's relevant to your project -->