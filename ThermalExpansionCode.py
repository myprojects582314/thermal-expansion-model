import numpy as np
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import tkinter as tk
from tkinter import *

# zakresy temp
Lc_t_range = np.array([
    [5, 100],
    [0, 100],
    [17, 100],
    [0, 100],
    [0, 100],
    [20, 600],
    [4, 100],
    [0, 150],
    [-35, 356],
    [-114, 78],
    [0, 100],
    [0, 100]
])

# przypisanie zakresów temp do materialów
Linear_temp_range = {
    'Aluminium': Lc_t_range[0],
    'Zelazo': Lc_t_range[1],
    'Zloto': Lc_t_range[2],
    'Nikiel': Lc_t_range[3],
    'Srebro': Lc_t_range[4],
    'Stal': Lc_t_range[5],
    'Woda': Lc_t_range[0],
    'Rtęć': Lc_t_range[1],
    'Benzyna': Lc_t_range[2],
    'Etanol': Lc_t_range[3],
    'Gliceryna': Lc_t_range[4],
    'Nafta': Lc_t_range[5]
}

Lc_values = np.array([23.1 * pow(10, -6),
                      11.8 * pow(10, -6),
                      14 * pow(10, -6),
                      13 * pow(10, -6),
                      18 * pow(10, -6),
                      12 * pow(10, -6),
                      69 * pow(10, -6),
                      60.5 * pow(10, -6),
                      316.7 * pow(10, -6),
                      366.7 * pow(10, -6),
                      166.7 * pow(10, -6),
                      346.5 * pow(10, -6)
                      ])

Linear_coefficient = {
    'Aluminium': Lc_values[0],
    'Zelazo': Lc_values[1],
    'Zloto': Lc_values[2],
    'Nikiel': Lc_values[3],
    'Srebro': Lc_values[4],
    'Stal': Lc_values[5],
    'Woda': Lc_values[0],
    'Rtęć': Lc_values[1],
    'Benzyna': Lc_values[2],
    'Etanol': Lc_values[3],
    'Gliceryna': Lc_values[4],
    'Nafta': Lc_values[5]
}


class Error_App(tk.Tk):
    def __init__(self, title="Aplikacja"):
        super().__init__()
        self.title(title)
        self.center()

    def run(self):
        self.mainloop()

    def center(self):
        self.update()
        wx = 300
        wy = 150
        sx = self.winfo_screenwidth()
        sy = self.winfo_screenheight()
        x = (sx - wx) // 2
        y = (sy - wy) // 2
        self.geometry("{}x{}+{}+{}".format(wx, wy, x, y))

    def close(self):
        self.quit()
        self.destroy()


class App(tk.Tk):
    # App dziedziczy z tkinter.Tk
    def __init__(self, title="Aplikacja"):
        super().__init__()
        self.title(title)
        self.center()

    def run(self):
        self.mainloop()

    def center(self):
        self.update()
        wx = 1000
        wy = 800
        sx = self.winfo_screenwidth()
        sy = self.winfo_screenheight()
        x = (sx - wx) // 2
        y = (sy - wy) // 2
        self.geometry("{}x{}+{}+{}".format(wx, wy, x, y))

    def lc_change_material(self, material='None'):
        napis0 = Label(app, text='  Wybierz temperature poczatkowa\n na ponizszym suwaku[°C]  ')
        napis0.grid(column=0, row=7, columnspan=2, pady=6)
        slider0 = Scale(app, from_=Linear_temp_range[material][0], to=Linear_temp_range[material][1], orient=HORIZONTAL,
                        activebackground='#DDA0DD', length=150, troughcolor='#D8BFD8')
        slider0.grid(column=0, columnspan=2, row=8, pady=6)
        napis1 = Label(app, text='  Wybierz temperature koncowa\n na ponizszym suwaku[°C]  ')
        napis1.grid(column=0, row=9, columnspan=2, pady=6)
        slider1 = Scale(app, from_=Linear_temp_range[material][0], to=Linear_temp_range[material][1], orient=HORIZONTAL,
                        activebackground='#DDA0DD', length=150, troughcolor='#D8BFD8')
        slider1.grid(column=0, columnspan=2, row=10, pady=6)
        napis2 = Label(app, text='  Wybierz dlugosc pocz preta\n na ponizszym suwaku[cm]    ')
        napis2.grid(column=0, row=11, columnspan=2, pady=6)
        slider2 = Scale(app, from_=1, to=500, orient=HORIZONTAL, activebackground='#DDA0DD', length=150,
                        troughcolor='#D8BFD8')
        slider2.grid(column=0, columnspan=2, row=12, pady=6)
        przycisk_run = tk.Button(app, text='Uruchom', width=20, bg='#D8BFD8',
                                 command=lambda: (App.Wykres_lc(self, material, T0=int(slider0.get()),
                                                                TK=int(slider1.get()),
                                                                x0=int(slider2.get())),
                                                  App.legenda_pret_lc(self, material,
                                                                      T0=int(slider0.get()),
                                                                      TK=int(slider1.get()),
                                                                      X0=int(slider2.get()))))
        przycisk_run.grid(row=13, column=0, columnspan=2, padx=5, pady=5)

    def vc_change_material(self, material='None'):
        napis0 = Label(app, text='Wybierz temperature poczatkowa\n na ponizszym suwaku[°C]')
        napis0.grid(column=0, row=7, columnspan=2, pady=5)
        slider0 = Scale(app, from_=Linear_temp_range[material][0], to=Linear_temp_range[material][1], orient=HORIZONTAL,
                        activebackground='#B0C4DE', length=150, troughcolor='#AFEEEE')
        slider0.grid(column=0, columnspan=2, row=8, pady=5)
        napis1 = Label(app, text='Wybierz temperature koncowa\n na ponizszym suwaku[°C]')
        napis1.grid(column=0, row=9, columnspan=2, pady=5)
        slider1 = Scale(app, from_=Linear_temp_range[material][0], to=Linear_temp_range[material][1], orient=HORIZONTAL,
                        activebackground='#B0C4DE', length=150, troughcolor='#AFEEEE')
        slider1.grid(column=0, columnspan=2, row=10, pady=5)
        napis2 = Label(app, text='Wybierz objętość pocz cieczy\n na ponizszym suwaku [cm^3]')
        napis2.grid(column=0, row=11, columnspan=2, pady=5)
        slider2 = Scale(app, from_=10, to=5000000, orient=HORIZONTAL, activebackground='#B0C4DE', length=150,
                        troughcolor='#AFEEEE')
        slider2.grid(column=0, columnspan=2, row=12, pady=5)
        przycisk_run = tk.Button(app, text='Uruchom', width=20, bg='#AFEEEE',
                                 command=lambda: (App.Wykres_vc(self, material, T0=int(slider0.get()),
                                                                TK=int(slider1.get()),
                                                                v0=int(slider2.get())),
                                                  App.legenda_pret_vc(self, material,
                                                                      T0=int(slider0.get()),
                                                                      TK=int(slider1.get()),
                                                                      v0=int(slider2.get()))))

        przycisk_run.grid(row=13, column=0, columnspan=2, padx=5, pady=5)

    def zapisz_dane_plik(self, material='None', T0=50, TK=50, v0='', a='', b='', c='', d='', e='', f=''):
        file = open(f'Wyniki_{material}_{str(T0)}_{str(TK)}_{str(v0)}.txt', 'w')
        file.write(f'{a} \n {b}\n {c}\n {d}\n {e}\n {f}\n')
        file.close()

    def legenda_pret_lc(self, material='None', T0=50, TK=50, X0=0):
        if T0 == TK:
            error = Error_App('Błąd')
            error.center()
            text = Label(error,
                         text='Temperatura końcowa i początkowa\n nie może być taka sama,\n spróbuj jeszcze raz!',
                         font=("Helvetica", 12))
            text.grid(column=1, row=1, padx=20, pady=5)
            button = tk.Button(error, text='OK', command=lambda: Error_App.close(error))

            button.grid(column=1, row=2, padx=30, pady=10)
            error.run()

        else:
            a = Linear_coefficient[material]
            y = float(X0) * (1 + a * float(TK - T0))
            space = ' '
            legenda0 = Label(app, text=f'Uzyty material: {material} {20 * space}')
            legenda1 = Label(app, text=f'Poczatkowa temperatura: {T0} [°C] {20 * space}')
            legenda2 = Label(app, text=f'Koncowa temperatura: {TK} [°C] {20 * space}')
            legenda3 = Label(app, text=f'Poczatkowa dlugosc preta: {round((X0 / 100), 10)} [m] {20 * space}')
            legenda4 = Label(app, text=f'Koncowa dlugosc preta: {round((y / 100), 10)} [m] {20 * space}')
            legenda5 = Label(app, text=f'Przyrost dlugosci preta: {round(((y - X0) / 100), 10)} [m] {20 * space}')
            legenda0.grid(column=3, row=0, sticky=W, padx=5)
            legenda1.grid(column=3, row=1, sticky=W, padx=5)
            legenda2.grid(column=3, row=2, sticky=W, padx=5)
            legenda3.grid(column=3, row=3, sticky=W, padx=5)
            legenda4.grid(column=3, row=4, sticky=W, padx=5)
            legenda5.grid(column=3, row=5, sticky=W, padx=5)
            zapisz_dane = Button(app, text='Zapisz wyniki do \n pliku tekstowego txt', bg='#CC66FF',
                                 command=lambda: App.zapisz_dane_plik(self, material, T0, TK, str(X0), a=legenda0.cget('text'),
                                                                      b=legenda1.cget('text'), c=legenda2.cget('text'),
                                                                      d=legenda3.cget('text'), e=legenda4.cget('text'),
                                                                      f=legenda5.cget('text')))
            zapisz_dane.grid(column=4, row=3, rowspan=2, pady=5)
            komentarz = Label(app, text=f'Wyniki zapisane zostaną w pliku txt w schemacie:\n'
                                        f'Wyniki_(material)_(T0)_(TK)_(X0).txt\n'
                                        f'material = Uzyty material\n'
                                        f'T0 = temp poczatkowa\n'
                                        f'TK = temp koncowa\n'
                                        f'X0 = dlugosc poczatkowa [cm]\n')
            komentarz.grid(column=5, row=2, rowspan=4, padx=5)

    def legenda_pret_vc(self, material='None', T0=50, TK=50, v0=0):
        if T0 == TK:
            error = Error_App('Błąd')
            error.center()
            text = Label(error,
                         text='Temperatura końcowa i początkowa\n nie może być taka sama,\n spróbuj jeszcze raz!',
                         font=("Helvetica", 12))
            text.grid(column=1, row=1, padx=20, pady=5)
            button = tk.Button(error, text='OK', command=lambda: Error_App.close(error))

            button.grid(column=1, row=2, padx=30, pady=10, columnspan=2)
            error.run()


        else:
            a = Linear_coefficient[material]
            y = float(v0) * (1 + 3 * a * float(TK - T0))
            space = ' '
            legenda0 = Label(app, text=f'Uzyty material: {material} {10 * space}')
            legenda1 = Label(app, text=f'Poczatkowa temperatura: {T0} [°C] {10 * space}')
            legenda2 = Label(app, text=f'Koncowa temperatura: {TK} [°C] {10 * space}')
            legenda3 = Label(app, text=f'Poczatkowa objetosc cieczy : {round((v0 / 1000000), 10)} [m^3] {10 * space}')
            legenda4 = Label(app, text=f'Koncowa objetosc cieczy: {round((y / 1000000), 10)} [m^3] {10 * space}')
            legenda5 = Label(app,
                             text=f'Przyrost objetosci cieczy: {round(((y - v0) / 1000000), 10)} [m^3] {10 * space}')
            legenda0.grid(column=3, row=0, sticky=W, padx=5)
            legenda1.grid(column=3, row=1, sticky=W, padx=5)
            legenda2.grid(column=3, row=2, sticky=W, padx=5)
            legenda3.grid(column=3, row=3, sticky=W, padx=5)
            legenda4.grid(column=3, row=4, sticky=W, padx=5)
            legenda5.grid(column=3, row=5, sticky=W, padx=5)
            zapisz_dane = Button(app, text='Zapisz wyniki do \n pliku tekstowego txt', bg='#33FFFF',
                                 command=lambda: App.zapisz_dane_plik(self, material, T0, TK, str(v0), a=legenda0.cget('text'),
                                                                      b=legenda1.cget('text'), c=legenda2.cget('text'),
                                                                      d=legenda3.cget('text'), e=legenda4.cget('text'),
                                                                      f=legenda5.cget('text')))
            zapisz_dane.grid(column=4, row=3, rowspan=2, pady=5)
            komentarz = Label(app, text=f'Wyniki zapisane zostaną w pliku txt w schemacie:\n'
                                        f'Wyniki_(material)_(T0)_(TK)_(V0).txt\n'
                                        f'material = Uzyty material\n'
                                        f'T0 = temp poczatkowa\n'
                                        f'TK = temp koncowa\n'
                                        f'V0 = objetosc poczatkowa [cm]\n')
            komentarz.grid(column=5, row=2, rowspan=4, padx=5)

    def Wykres_lc(self, material='None', T0=50, TK=50, x0=50):
        if T0 != TK:
            fig = Figure(figsize=(7, 5), dpi=100)
            a = Linear_coefficient[material]
            x = np.linspace(float(T0), float(TK))
            y = float(x0) * (1 + a * (x - T0))

            plot1 = fig.add_subplot(111)
            plot1.plot(x, y, color='#DC143C', lw=2, linestyle=':')

            for label in plot1.xaxis.get_ticklabels():
                label.set_color('black')
                label.set_rotation(0)
                label.set_fontsize(7)

            for label in plot1.yaxis.get_ticklabels():
                label.set_color('black')
                label.set_rotation(45)
                label.set_fontsize(7)

            plot1.set_title('Wykres zależności długości pręta od temperatury')
            plot1.set_xlabel('Temperatura[°C]')
            plot1.set_ylabel('Dlugosc [m]')
            plot1.xaxis.grid(linestyle='dotted')
            plot1.yaxis.grid(linestyle='dotted')
            canvas = FigureCanvasTkAgg(fig, master=app)
            canvas.draw()
            canvas.get_tk_widget().grid(column=3, row=7, rowspan=250, columnspan=250, sticky=W)
            przycisk_zapisz = tk.Button(app, text='Zapisz wykres do pliku', width=20, bg='#CC66FF',
                                        command=lambda: fig.savefig(f'Wykres_dla_{material}_{T0}_{TK}_{x0}'))
            przycisk_zapisz.grid(row=14, column=0, columnspan=2, padx=5, pady=5)
            komentarz = Label(app, text='Wykres zapisuje sie w ponizszym schemiacie:\n'
                                        'Wykres_dla_(material)_(T0)_(TK)_(X0).jpg\n'
                                        'material = uzyty material\n'
                                        'T0 = temperatura poczatkowa\n'
                                        'TK = temperatura koncowa\n'
                                        'X0 = dlugosc poczatkowa [cm]\n')
            komentarz.grid(row=15, column=0, columnspan=2, padx=5, pady=5, sticky=W)

    def Wykres_vc(self, material='None', T0=50, TK=50, v0=50):
        if T0 != TK:
            fig = Figure(figsize=(7, 5), dpi=100)
            a = Linear_coefficient[material]
            x = np.linspace(float(T0), float(TK))
            y = (float(v0) * (1 + (3 * a) * (x - T0))) / 1000000
            plot1 = fig.add_subplot(111)
            plot1.plot(x, y, color='#20B2AA', lw=2, linestyle='dashdot')

            for label in plot1.xaxis.get_ticklabels():
                label.set_color('black')
                label.set_rotation(0)
                label.set_fontsize(7)

            for label in plot1.yaxis.get_ticklabels():
                label.set_color('black')
                label.set_rotation(45)
                label.set_fontsize(7)

            plot1.set_title('Wykres zależności objętości cieczy od temperatury')
            plot1.set_xlabel('Temperatura[°C]')
            plot1.set_ylabel('Objetosc [m^3]')
            plot1.xaxis.grid(linestyle='dotted')
            plot1.yaxis.grid(linestyle='dotted')
            canvas = FigureCanvasTkAgg(fig, master=app)
            canvas.draw()
            canvas.get_tk_widget().grid(column=3, row=7, rowspan=250, columnspan=250, sticky=W)
            przycisk_zapisz = tk.Button(app, text='Zapisz wykres do pliku', width=20, bg='#33FFFF',
                                        command=lambda: fig.savefig(f'Wykres_dla_{material}_{T0}_{TK}_{v0}'))
            przycisk_zapisz.grid(row=14, column=0, columnspan=2, padx=5, pady=5)
            komentarz = Label(app, text='Wykres zapisuje sie w ponizszym schemiacie:\n'
                                        'Wykres_dla_(material)_(T0)_(TK).jpg\n'
                                        'material = uzyty material\n'
                                        'T0 = temperatura poczatkowa\n'
                                        'TK = temperatura koncowa\n'
                                        'V0 = objetosc poczatkowa [cm^3]\n')
            komentarz.grid(row=15, column=0, columnspan=2, padx=5, pady=5, sticky=W)


app = App('Zjawisko rozszerzalnosci cieplnej')

button = lambda material: tk.Button(app, text=material, width=10, activebackground='#DDA0DD', bg='#D8BFD8',
                                    command=lambda: app.lc_change_material(material))
button_obj = lambda material: tk.Button(app, text=material, width=10, activebackground='#B0C4DE', bg='#AFEEEE',
                                        command=lambda: app.vc_change_material(material))

legenda0 = Label(app, text=f'Ciało stałe,\n rozszerzalność liniowa')
legenda1 = Label(app, text=f'Ciecz,\n rozszerzalność objętościowa')

legenda0.grid(column=0, row=0)
legenda1.grid(column=1, row=0)

button('Aluminium').grid(row=1, column=0, padx=5, pady=5)
button('Zelazo').grid(row=2, column=0, padx=5, pady=5)
button('Zloto').grid(row=3, column=0, padx=5, pady=5)
button('Nikiel').grid(row=4, column=0, padx=5, pady=5)
button('Srebro').grid(row=5, column=0, padx=5, pady=5)
button('Stal').grid(row=6, column=0, padx=5, pady=5)

button_obj('Woda').grid(row=1, column=1, padx=5, pady=5)
button_obj('Rtęć').grid(row=2, column=1, padx=5, pady=5)
button_obj('Benzyna').grid(row=3, column=1, padx=5, pady=5)
button_obj('Etanol').grid(row=4, column=1, padx=5, pady=5)
button_obj('Gliceryna').grid(row=5, column=1, padx=5, pady=5)
button_obj('Nafta').grid(row=6, column=1, padx=5, pady=5)

app.run()
